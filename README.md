# WMF Phabricator Task Dates

This application queries for the start, modified, and end dates of Phabricator tasks based on task id and/or tags.

![screenshot](wmf-phabricator-task-dates-form.png "screenshot")
![screenshot](wmf-phabricator-task-dates-results.png "screenshot")

## Usage

- Make sure [Docker](https://www.docker.com/products/docker-desktop/) is installed.
- Specify the api token in the `.env` file at the root of this project.
- Because the app is run from localhost, the easiest way to avoid CORS errors in order to make API calls to [Phabricator Conduit](https://phabricator.wikimedia.org/conduit/) is to use a browser extension like [CORS Unblock](https://chromewebstore.google.com/detail/lfhmikememgdcahcdlaciloancbhjino?hl=en). Otherwise you will encounter CORS errors when trying to submit the form.
- Run the following commands to launch the app:
    - `docker compose up -d` to start the containers.
    - `docker exec -it vite_docker sh` to ssh into the container.
    - `npm install` from inside the container
    - `npm run dev` from inside the container
    - navigate to `http://localhost:8008/`

### Notes

The token is used to request data from the Phabricator API. Generate an API token via a link formatted like the following: 

`https://phabricator.wikimedia.org/settings/user/<username>/page/apitokens/`